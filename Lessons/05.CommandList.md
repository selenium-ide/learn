# Danh sách các lệnh cần thiết trong Selenium IDE

|Command|Target|Value|Ý nghĩa|Link Tham khảo|
|--|--|--|--|--|
|**`open`**|**`/`** : để truy cập đến trang home của Base Url<br>(Luôn là command đầu tiên mỗi 1 test case)<br>**`/Link-trong`** : để truy cập vào cấp trong của Base Url||Để mở về trang home hoặc các trang trong của Base Url|[Xem tại đây](https://www.qafox.com/new-selenium-ide-open-command/)|
|**`Type`**|[Locators](https://gitlab.com/selenium-ide/learn/-/blob/master/Lessons/04.Locators.md)||Gõ vào ô input với giá trị ở ô **`value`**|[Xem tại đây](https://www.qafox.com/new-selenium-ide-type-command/)| 
|**`click`**|[Locators](https://gitlab.com/selenium-ide/learn/-/blob/master/Lessons/04.Locators.md)||Để thực hiện thao tác Click chuột vào các link, nút,...|[Xem tại đây](https://www.qafox.com/new-selenium-ide-click-command/)| 
|**`select`**|[Locators](https://gitlab.com/selenium-ide/learn/-/blob/master/Lessons/04.Locators.md)|Giá trị của 1 option|Dùng để chọn 1 giá trị option trong Select xổ xuống|[Xem tại đây](https://www.qafox.com/new-selenium-ide-using-select-command-for-selecting-a-dropdown-option/)| 
|**`add selection`**|[Locators](https://gitlab.com/selenium-ide/learn/-/blob/master/Lessons/04.Locators.md)|Giá trị của 1 option|Thêm 1 giá trị option nữa ở SELECT multiple|[Xem tại đây](https://www.qafox.com/new-selenium-ide-using-add-selection-command-for-selecting-multi-selection-box-field-options/)| 
|**`remove selection`**|[Locators](https://gitlab.com/selenium-ide/learn/-/blob/master/Lessons/04.Locators.md)|Giá trị của 1 option|Xóa 1 lựa chọn của Select multiple|[Xem tại đây](https://www.qafox.com/new-selenium-ide-using-remove-selection-command/)| 
|**`check`**|[Locators](https://gitlab.com/selenium-ide/learn/-/blob/master/Lessons/04.Locators.md)||đánh dấu(tích, chọn) giá trị ở check , radio|[Xem -Radio](https://www.qafox.com/new-selenium-ide-using-check-command-for-selecting-a-radio-button/)<br>[Xem -Check](https://www.qafox.com/new-selenium-ide-using-check-command-for-selecting-a-checkbox-option/)| 
|**`uncheck`**|[Locators](https://gitlab.com/selenium-ide/learn/-/blob/master/Lessons/04.Locators.md)||bỏ đánh dấu(tích, chọn) giá trị ở check , radio|[Xem -Radio](https://www.qafox.com/new-selenium-ide-using-uncheck-command-for-deselecting-a-radio-button/)<br>[Xem-Check](https://www.qafox.com/new-selenium-ide-using-uncheck-command-for-deselecting-a-checkbox-option/)| 
|**`echo`**|text,giá trị, biến chứa hóa trị||Dùng để hiển thị thông báo ở phần Log panel của Selenium IDE|[Xem tại đây](https://www.qafox.com/new-selenium-ide-using-echo-command/)| 
|**`pause`**|Nhập số ở đây, đơn vị là mili giây<br>Ví dụ: **`10000`** = 10 giây||Tạm dừng 1 khoảng thời gian (đơn vị mili giây)<br>**`1000`** mili giây =  **`1`** giây|[Xem tại đây](https://www.qafox.com/new-selenium-ide-using-pause-command/)| 
|**`submit`**|[Locators](https://gitlab.com/selenium-ide/learn/-/blob/master/Lessons/04.Locators.md)||Thay vì ấn nút Submit bằng click chuột, ta thêm lệnh này |[Xem tại đây](https://www.qafox.com/new-selenium-ide-using-submit-command/)| 

# Tham khảo

- Các lệnh khác [Tham khảo tại đây](https://www.qafox.com/new-selenium-ide-commands-selenese/)

<!-- |**`Type`**|[Locators](https://gitlab.com/selenium-ide/learn/-/blob/master/Lessons/04.Locators.md)||Text|[Xem tại đây](LINK)|  -->
<!-- |**`command`**|[TITLE](LINK)||Text|[Xem tại đây](LINK)|  -->